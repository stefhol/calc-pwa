import React from "react";
interface UserContext {
    user:User
    setUser: Function
}
export interface User {

    id: string,
    history: boolean

}
const user = { id: "1", history: true }
export const UserContext = React.createContext<Partial<UserContext>>({ user });