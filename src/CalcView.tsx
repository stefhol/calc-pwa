import React, { useReducer, useEffect, useMemo, useContext } from 'react';
import { Button,  Collapse, } from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import { CalcHistory } from './CalcHistory';
import { UserContext } from './UserContext';

/**
 * Interface for the calculator reducer
 */
export interface calc_core {
    num: string;
    sec_num: string;
    task: string;
    result: number;
}
export interface calc_history {
    history: calc_core[] | []
}
interface calc_interface extends calc_core, calc_history {
}
/**
 * Performes calculation on the reducer state
 * @param state : num, sec_num and task required
 */
const doCalc = (state: calc_interface) => {
    let result;
    let num = Number.parseFloat(state.num);
    let sec_num = Number.parseFloat(state.sec_num);
    switch (state.task) {
        case "*":
            result = num * sec_num;
            break;
        case "-":
            result = num - sec_num;

            break;
        case "+":
            result = num + sec_num;

            break;
        case "/":
            result = num / sec_num;

            break;
        default:
            break;
    }
    return result;
}
/**
 * set function for the reducer
 * @param state old state
 * @param action object that carries new data
 */
const set_calculator = (state: calc_interface, action: { input?}) => {
    let lstate = state;
    let num;
    switch (action.input) {
        case "*":
        case "+":
        case "/":
        case"-":
            //After result calculated and press +
            if (lstate.num === "" && lstate.result !== 0) {
                return { ...lstate, result: 0, task: action.input, num: String(lstate.result) }
            }
            //default change operator
            else {
                return { ...lstate, task: action.input }
            }
        case ".":
            if (lstate.sec_num !== "") {
                let num = lstate.sec_num;
                //check if . is already part of string
                if (num.indexOf(".")!==-1) {
                    //yes return previous state
                    return { ...lstate }
                }
                else {
                    //no return new state
                    return { ...lstate, sec_num: num + action.input }
                }
            }
            else {
                let num = lstate.num;
                //check if . is already part of string
                if (num.indexOf(".")!==-1) {
                    //yes return previous state
                    return { ...lstate }
                }
                else {
                    //no return new state
                    return { ...lstate, num: num + action.input }
                }
            }
        case "equals":
            //check if state is correctly set for calc
            if (lstate.sec_num !== "" && lstate.task !== "") {
                num = doCalc(lstate)
                let historyObj = JSON.parse(JSON.stringify({ ...state, result: num }));
                delete historyObj.history;
                return { ...initital_calculator, result: num, task: "", history: [...state.history, historyObj] }
            }
            return { ...lstate }
        case "C":
            if (action.input === "C") {
                return initital_calculator;
            }
            return { ...lstate }
        case "back":
            //remove last char
            if (lstate.sec_num) {
                return { ...lstate, sec_num: lstate.sec_num.slice(0, -1) }
            }
            else {
                return { ...lstate, task: "", num: lstate.num.slice(0, -1) }
            }
        default:
            //perform add on num or sec_num string
            if (lstate.task === "") {
                num = lstate.num + action.input
                return { ...lstate, num }
            }
            else {
                num = lstate.sec_num + action.input
                return { ...lstate, sec_num: num }
            }
    }
}
/**
 * Handles Buttons presses from the keyboard
 * @param e 
 */

/**
 * Creates the button order note if you want to add another 
 * column change the grid template in the css
 */
const buttonOrder = [
    1, 2, 3, '*',
    4, 5, 6, "+",
    7, 8, 9, "/",
    "C", 0, ".", "equals"
]
/**
 * matches icons to given string
 * @param str 
 */
const iconMatch = (str: string | number) => {
    let value;
    switch (str) {
        case "back":
            value = <ArrowBack />
            break;
        case "equals":
            value = "="
            break;
        default:
            value = str
            break;
    }
    return value
}

const initital_calculator: calc_interface = {
    num: "", task: "", sec_num: "", result: 0, history: []
};
interface CalcViewProps {

}
export const CalcView: React.FC<CalcViewProps> = (props) => {
    const {user} = useContext(UserContext);
    const [calculator_state, dispatch] = useReducer(set_calculator, initital_calculator);
    // static create function prevents unneccesary rerendering
    /**
     * Creates the button array out of an array
     * @param array [2,"test",3] 
     */
    const createButtons = (array: Array<string | number>) => {
        const buttons = array.map((el) => {
            return (<div key={el} className={"num-btn " + el}>
                <Button className = {"num-btn"} onClick={e => {
                    e.preventDefault();
                    handleClick(el);
                }
                }>
                    {iconMatch(el)}
                </Button>
            </div>)
        })
        return buttons;
    }
    /**
     * called on button click in the calculator buttons
     * @param input 
     */
    const handleClick = (input: string | number) => {
        dispatch({ input: input })
    }
    /**
     * creates the output html structure
     */
    const createOutput = () => {
        let output = [];
        for (const key in calculator_state) {
            if (calculator_state.hasOwnProperty(key)) {
                let element;
                if (key === "result") {
                    element = " = " + calculator_state[key];
                }
                else if (key === "history") {
                    break;
                }
                else {
                    element = calculator_state[key];
                }
                output.push(
                    <div key={key}
                        className={key}
                    >
                        {element}
                    </div>
                )
            }
        }
        return output
    }
    const toggleKeyboard = (bool) => {
        document.removeEventListener('keydown', handleKeyDown, false)
        if (bool) {
            document.addEventListener('keydown', handleKeyDown, false)
        }
        
    }
    const handleKeyDown = (e) => {
        console.log(e.key)
        if (!e) { return; }
        let defaultAction = () => {
            let key = e.key;
            switch (e.key) {
                case ",":
                    key = "."
                    break;
                case "Escape":
                case "c":
                    key = "C"
                    break;
                case "Enter":
                case "=":
                    key = "equals"
                    break;
                case "Backspace":
                    key = "back"
                    break;
            }
            dispatch({ input: key })
        }
        /**
         * Key listener
         */
        const keyAction = {
            1: { action: defaultAction },
            2: { action: defaultAction },
            3: { action: defaultAction },
            4: { action: defaultAction },
            5: { action: defaultAction },
            6: { action: defaultAction },
            7: { action: defaultAction },
            8: { action: defaultAction },
            9: { action: defaultAction },
            0: { action: defaultAction },
            "=": { action: defaultAction },
            "*": { action: defaultAction },
            "-": { action: defaultAction },
            "+": { action: defaultAction },
            "/": { action: defaultAction },
            ".": { action: defaultAction },
            ",": { action: defaultAction },
            Enter: { action: defaultAction },
            Backspace: { action: defaultAction },
            c: { action: defaultAction },
            C: { action: defaultAction },
            Escape: { action: defaultAction },
        };
        if (e.key && e.key in keyAction) {
            e.preventDefault();
            keyAction[e.key]["action"]();
        }
    }
    useEffect(() => {
        toggleKeyboard(true)
        return () => {
            toggleKeyboard(false)
        };
    }, [])
    //Prevents unneccesarry rerenders of static buttons
    const memo_createButtons = useMemo(() => createButtons(buttonOrder), []);
    return (
        <>
            <div className="calculator"
                onKeyDown={e => handleKeyDown(e)}>
                <div className="output">
                    {createOutput()}
                </div>
                <div className="calc">
                    {memo_createButtons}
                </div>
            </div>
            <div className = "outer-history">
            {/* <Collapse appear  in = {user.history}> */}
            <CalcHistory history={calculator_state.history} />
            {/* </Collapse> */}
            </div>
        </>
    )
}
