import React, { useEffect, useState } from 'react'
import { calc_history } from './CalcView'
import {Paper} from "@material-ui/core"
export interface CalcHistoryProps extends calc_history{
}
export const CalcHistory: React.FC<calc_history> = (props) => {
    const [state, setstate] = useState([])
    const createHistoryHtml = () => {
        let historyJSX = [];
        props.history.forEach(el => {
            historyJSX.push(
            <Paper elevation = {5} key={el.num}>
            <div key = {el.num+el.result} className = "calc-history-item">
                {el.num + el.task + el.sec_num + "=" + el.result}
            </div>
            </Paper>
            )
        })
        console.log(historyJSX)
        return historyJSX
    }
    useEffect(() => {
        setstate(createHistoryHtml())
    },[props.history])
    return (<div className = "calculator-history">
        <div className = "calc-history">
        {state}
        </div>
    </div>)
}