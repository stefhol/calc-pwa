import React, { useState, useContext } from "react"
import { Toolbar, IconButton, Typography, Button, AppBar, createStyles, makeStyles, Theme, Switch, FormControlLabel } from "@material-ui/core"
import { Menu } from "@material-ui/icons";
import { UserContext } from "./UserContext";

export interface AppBarProps { }
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: "80px",
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }),
);
export const NavigationBar: React.FC<AppBarProps> = (props) => {
    const [historyCheck, setHistoryCheck] = useState(true);
    const {user , setUser} = useContext(UserContext)
    const changeHistory = () => {
        setUser({...user,history:!historyCheck})
        setHistoryCheck(!historyCheck);
    }
    const classes = useStyles();
    return (
        <AppBar className={classes.root+'appbar'}>
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <Menu />
                </IconButton>
                <FormControlLabel
                    control={<Switch checked={historyCheck} onChange={() => changeHistory()} name="checkedA" />}
                    label="History"
                />
                <Typography variant="h6" className={classes.title}>
                    News
      </Typography>
                <Button color="inherit">Login</Button>
            </Toolbar>
        </AppBar>
    )
}