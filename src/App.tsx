import React, { useState } from 'react';
import { UserContext } from './UserContext';
import { CalcView } from './CalcView';
import './css/index.scss'
import { NavigationBar } from "./NavigationBar";


interface AppProps { }
const App: React.FC<AppProps> = (props) => {
    const [user, setUser] = useState({id:"1",history:true});
    
    return (
        <UserContext.Provider value={{ user, setUser }}>
        <div>
            <NavigationBar/>
        </div>
            <div className = "calculator-box">
                <CalcView/>
                </div>
        </UserContext.Provider>
    )
}
export default App;
